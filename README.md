# What?

Very simple tool initially to organise Victor Dev's "On-Call" Rota. Hacked
together in a few hours with no much thought for anything except its primary
usage.

Capable of randomly assigning Victor's 5 full stack developers to each day of
the week, alerting everybody in the main group room and adding the on-call
developer to the daily support room daily.

# TODO
- [ ] Fix TODOs
- [ ] Unit tests
- [ ] Gitlab-CI
