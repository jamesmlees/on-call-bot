var glob = require( 'glob' ),
    mongoose = require( 'mongoose' ),
    Promise = require( 'bluebird' );

/**
 * initialises database connection based on env var, replaces mongoose promise
 * library with bluebird!
 */
function initDb(){
  mongoose.connect('mongodb://' + process.env.DB_URI );
  mongoose.Promise = Promise;
}

/**
 * Loads all mongoose models matching *-model.js in any directory
 */
function loadModels(){
  var models = glob.sync( '**/*-model.js' );

  if( !models.length ) return;
  models.forEach( function( model ){
    try{
      //TODO should probably use fs here and avoid this try catch
      require( './' + model );
    }
    catch( e ){
      if( e ) console.error( e );
    }
  });
}

/**
 * initialises dbs and odels
 */
function init(){
  initDb();
  loadModels();
}

init();
