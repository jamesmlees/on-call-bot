require( 'dotenv' ).config();
require( './config' );

var timetables = require( './lib/timetables' ),
    constants = require( './lib/constants' ),
    messaging = require( './lib/messaging' ),
    scheduler = require( './scheduling-logic' );

scheduler.init();
