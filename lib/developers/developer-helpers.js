var mongoose = require( 'mongoose' ),
    Developer = mongoose.model( 'Developer' ),
    Promise = require( 'bluebird' );

/**
 * Query developer collection
 *
 * @param {Object} query
 * @returns {Promise} resolves with result of query 
 *
 */
function getDevelopers( query ){
  query = query || {};
  return Developer.find( query ).exec();
}

/**
 * Get the developer currently set to currentlyInterruptable
 *
 * @param {Object} query
 * @returns {Promise} resolves with current on call developer
 *
 */
function getCurrentOnCallDeveloper(){
  return getDevelopers( { currentlyInterruptable: true } )
    .then( function( devs ){
      if( devs.length ) return devs[ 0 ];
      return;
    });
}

/**
 * Switch the on call developer from the current to the provided
 *
 * @param {Object} newOnCall - dev object of dev to set as interruptable
 * @returns {Promise} resolving to object with newOnCall/oldOnCall keys where
 * available
 *
 */
function switchOnCall( newOnCall ){
  if( !newOnCall ) return Promise.resolve({});
  return getCurrentOnCallDeveloper().then( function( oldOnCall ){

    // if the newOnCall dev is the same as the old one, we don't need to update
    // any records
    if( newOnCall.id === oldOnCall.id ){
      return { newOnCall: newOnCall };
    }

    var promises = {};
    newOnCall.currentlyInterruptable = true;
    promises.newOnCall = newOnCall.save();

    if( oldOnCall ){
      oldOnCall.currentlyInterruptable = false;
      promises.oldOnCall = oldOnCall.save();
    }
    return Promise.props( promises );
  });
}


exports.getCurrentOnCallDeveloper = getCurrentOnCallDeveloper;
exports.getDevelopers = getDevelopers;
exports.switchOnCall = switchOnCall;
