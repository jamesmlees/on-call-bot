var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var developerSchema = new Schema({
  hipchatId: String,
  name: String,
  mentionName: String,
  currentlyInterruptable: Boolean
});

var Deveveloper = mongoose.model( 'Developer', developerSchema );
