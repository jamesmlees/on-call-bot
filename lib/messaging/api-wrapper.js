var rp = require( 'request-promise' );

/**
 * Light wrapper around bitbucket api
 *
 * @param {String} query
 * @param {Object} opts - optional options object
 *
 */
module.exports = function( query, opts ){
  opts = opts || {};
  var reqData = {
    qs: {
      auth_token: process.env.HIPCHAT_TOKEN,
    },
    uri: process.env.BASE_URI + query,
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true,
    method: opts.method || 'GET',
    body: opts.body || {}
  };
  if( opts.authTest ) reqData.qs.auth_test = true;
  return rp( reqData ).then( function( data ){
    return data;
  })
  .catch( function( err ){
    console.error( err.stack || err );
    return err;
  });
};

