var api = require( './api-wrapper' ),
    _ = require( 'lodash' );

/**
 * Send a simple message to <room>
 *
 * @param {String} room
 * @param {Srtring} message - simple message body
 *
 */
function sendMessage( room, message ){
  api( `/room/${room}/message`, {
    method: "POST",
    body: message
  });
}

/**
 * Send a notification to <room>, notification is basically a message with
 * customisable style.
 *
 * @param {String} room
 * @param {Object} messageObj https://www.hipchat.com/docs/apiv2/method/send_room_notification
 *
 */
function notifyRoom( room, messageObj ){
  var notification =  _.extend({
    from: "on-call-bot",
    color: "purple",
    notify: "true",
  }, messageObj );

  api( `/room/${room}/message`, {
    method: "POST",
    body: notification
  });
}

/**
 * Add <user> to <room> in hipchat
 *
 * @param {String} query
 * @param {Object} user - instance of user model ( can be model or object )
 *
 */
function addUser( room, user ){
  var opts = {
    method: 'PUT'
  };
  api( `/room/${room}/member/${user.hipchatId}`, opts );
}

/**
 * Remove <user> from <room>
 *
 * @param {String} room
 * @param {Object} user - instance of user model ( can be model or object )
 *
 */
function removeUser( room, user ){
  var opts = {
    method: 'DELETE'
  };
  api( `/room/${room}/member/${user.hipchatId}`, opts );
}

exports.addUser = addUser;
exports.removeUser = removeUser;
exports.notifyRoom = notifyRoom;
exports.sendMessage = sendMessage;
