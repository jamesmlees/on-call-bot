var _ = require( 'lodash' ),
    mongoose = require( 'mongoose' ),
    Timetable = mongoose.model( 'Timetable' ),
    constants = require( '../constants' ),
    devHelpers = require( '../developers' ).helpers,
    Promise = require( 'bluebird' );

/**
 * Retrieves the currently active timetable, if no timetable is set, sets one
 *
 * @returns {Promise} resolving to the currently active timetable
 *
 */
function getCurrentTimetable(){
  return Timetable.find( { current: true } ).exec()
    .then( function( timetables ){
      if( timetables.length ) return timetables[ 0 ];
      return setNewTimetable();
    });
}

/**
 * Builds a new weekly timetable by shuffling developers and assigned each to a
 * day
 *
 * @returns {Promise} resolving to new timetable
 *
 */
function buildNewTimetable(){
  var timetable = new Timetable();
  var schedule = timetable.schedule;
  var days = constants.weekDays;

  return devHelpers.getDevelopers().then( function( devs ){
    devs = _.shuffle( devs );
    _.each( days, function( day, d ){
      schedule[ day ] = devs[ d ].mentionName;
    });
    return timetable;
  });
}

/**
 * Set a new weekly timetable
 *
 * @returns {Promise} resolving to new applied timetable
 *
 */
function setNewTimetable(){
  return Promise.all([
    buildNewTimetable(),
    getCurrentTimetable()
  ]).then( function( results ){
    var newTimetable = results[ 0 ];
    var existingTimetable = results[ 1 ] && results[ 1 ][ 0 ];

    newTimetable.current = true;
    newTimetable.setAt = new Date();
    if( !existingTimetable ){
      return newTimetable.save();
    }

    existingTimetable.current = false;
    existingTimetable.unsetAt = new Date();

    return existingTimetable.save().then( function( doc ){
      return newTimetable.save();
    })
    .then( function( timetables ){
      if( timetables.length ) return timetables[ 0 ];
    })
    .catch( function( err ){
      console.error( 'Problem saving new timetable, resetting existing' );
      existingTimetable.current = true;
      existingTimetable.setAt = new Date();
      existingTimetable.save();
      return err;
    });
  });
}

exports.setNewTimetable = setNewTimetable;
exports.getCurrentTimetable = getCurrentTimetable;
exports.buildNewTimetable = buildNewTimetable;

