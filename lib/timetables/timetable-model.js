var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;


var timetableSchema = new Schema({
  setAt: Date,
  unsetAt: Date,
  current: Boolean,
  schedule: {
    monday: { type: Object, default: '' },
    tuesday: { type: Object, default: '' },
    wednesday: { type: Object, default: '' },
    thursday: { type: Object, default: '' },
    friday: { type: Object, default: '' },
  }
});

var Timetable = mongoose.model( 'Timetable', timetableSchema );
