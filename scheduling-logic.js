var schedule = require( 'node-schedule' );
var constants = require( './lib/constants' );
var devHelpers = require( './lib/developers' ).helpers;
var timetableHelpers = require( './lib/timetables' ).helpers;
var messagingHelpers = require( './lib/messaging' ).helpers;

exports.init = function(){
  var messageSchedule = schedule.scheduleJob('0 45 9 * * 1-5', function(){
    changeover();
  });

  var setSchedule = schedule.scheduleJob('0 0 12 * * 0', function(){
    // TODO currently operating on a fixed wekly timetable
    //timetableHelpers.setNewTimetable();
  });
};

/**
 * Gets the on call developer for today, based on the current timetable
 *
 * @returns {Promise} resolves with new developer on call developer
 *
 */
function getNewOnCallDeveloper(){
  return timetableHelpers.getCurrentTimetable()
    .then( function( timetable ){
      var day = constants.weekDays[ new Date().getDay() - 1 ];
      var query = { mentionName: timetable.schedule[ day ] };
      return devHelpers.getDevelopers( query );
    }).then( function( devs ){
      return devs && devs[ 0 ];
    });
}

/**
 * Replaces the previous on call developer with the new one as per timetable
 * sends a message to MAIN_ROOM and SUPPORT_ROOM alerting the on-call developer
 * and the the rest of the team
 *
 * @returns {Promise} resolves with result of query 
 *
 */
function changeover(){
  var supportRoom = process.env.SUPPORT_ROOM_NAME;
  var mainRoom = process.env.MAIN_ROOM_NAME;

  getNewOnCallDeveloper().then( function( newOnCall ){
    if( !newOnCall ){
      console.error( 'Unable to get new on call developer' );
      return;
    }
    return devHelpers.switchOnCall( newOnCall );
  })
  .then( function( switchObj ){
    var mainRoomMsg, supportRoomMsg;
    var newOnCall = switchObj.newOnCall;
    supportRoomMsg = `Paging @${newOnCall.mentionName}! (notsureifgusta)`;
    mainRoomMsg = `The on-call developer for today is ${newOnCall.name}.` +
      `@${newOnCall.mentionName} has been added to ${supportRoom}. ` +
      `Please go there for production enquiries/general disruptions, etc`;

    messagingHelpers.notifyRoom( mainRoom, { message: mainRoomMsg } );
    messagingHelpers.sendMessage( supportRoom, { message: supportRoomMsg } );
  });
}

