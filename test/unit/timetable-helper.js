'use strict';
/*global describe, it, before, beforeEach, after, afterEach */

var sinon = require('sinon'),
    Promise = require('bluebird'),
    mongoose = require( 'mongoose' ),
    rewire = require( 'rewire' ),
    _ = require('lodash'),
    chai = require( 'chai' ),
    should = chai.should(),
    sinonChai = require( 'sinon-chai' );

chai.use( sinonChai );


describe('timetable helpers', function(){
  var helper, modelStub, devHelper, timetableStub, timetable, devHelperStub,
  devs, currentTimetable, getCurrentTimetableResult;

  beforeEach( function(){
    modelStub = sinon.stub( mongoose, 'model' );
    currentTimetable = { current: true };
    getCurrentTimetableResult = Promise.resolve( [ currentTimetable ] );

    // TODO this isn't very nice
    timetableStub = sinon.spy( function(){
      this.schedule = {
        monday: '',
        tuesday: '',
        wednesday: '',
        thursday: '',
        friday: '',
      };
    });

    timetableStub.find = sinon.stub().returns( {
      exec: sinon.stub().returns( getCurrentTimetableResult )
    });

    modelStub.returns( timetableStub );
    helper = rewire('../../lib/timetables/timetable-helpers');
    devHelper = require('../../lib/developers/developer-helpers');
  });

  afterEach( function(){
    modelStub.restore();
  });

  it('should load the timetable model', function(){
    modelStub.should.have.been.called;
    var arg = modelStub.getCall( 0 ).args[ 0 ];
    arg.should.be.eql( 'Timetable' );
  });

  describe('getCurrentTimetable', function(){
    it('should retrieve the timetable set to current', function( done ){
      helper.getCurrentTimetable().then( function( timetable ){
        timetableStub.find.should.have.been.called;
        var query = timetableStub.find.getCall( 0 ).args[ 0 ];
        query.should.be.eql( { current: true } );
        timetable.should.be.eql( currentTimetable );
        done();
      }).catch( done );
    });

    // TODO should probably test this
    it.skip('should set a new timetable if no current one is found', function( done ){
    });
  });

  describe('buildNewTimetable', function(){
    var devHelperStub, devs;
    beforeEach( function(){
      devs = [
        { mentionName: 'nameOne' },
        { mentionName: 'nameTwo' },
        { mentionName: 'nameThree' },
        { mentionName: 'nameFour' },
        { mentionName: 'nameFive' }
      ];
      devHelperStub = sinon.stub( devHelper, 'getDevelopers' );
      devHelperStub.returns( Promise.resolve( devs ) );
    });
    afterEach( function(){
      devHelperStub.restore();
    });

    it('should create a new timetable instance', function(){
      return helper.buildNewTimetable().then( function( timetable ){
        timetableStub.should.be.called;
      });

    });
    it('should retrieve all developers', function(){
      return helper.buildNewTimetable().then( function( timetable ){
        devHelperStub.should.be.called;
      });
    });
    it('should assign a developer to each day', function(){
      var days = [ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday' ];
      return helper.buildNewTimetable().then( function( timetable ){
        timetable.should.include.key( 'schedule' );
        var schedule = timetable.schedule;
        schedule.should.have.keys( days  );
        allValuesDeveloperName( schedule, devs ).should.be.eql( true );
      });

      function allValuesDeveloperName( schedule, devs ){
        return _.every( schedule, function( val ){
          return _.find( devs, { mentionName: val } );
        });
      }
    });
    it('should shuffle developers', function(){
      return helper.buildNewTimetable().then( function( timetable ){
        someValuesShuffled( timetable.schedule, devs ).should.be.eql( true );

        function someValuesShuffled( schedule, devs ){
          var i = -1;
          return _.some( schedule, function( val ){
            i++;
            return schedule[ val ] !== devs[ i ].mentionName;
          });
        }
      });
    });
  });
  describe('setNewTimetable', function(){
    var buildNewTimetableStub, getCurrentTimetableStub, mockOldTimetable,
    mockNewTimetable, savedTimetable;

    beforeEach( function(){

      savedTimetable = [{ schedule: [] }];
      mockOldTimetable = {
        save: sinon.stub().returns( Promise.resolve() ),
        current: true,
      };
      mockNewTimetable = {
        save: sinon.stub().returns( Promise.resolve( savedTimetable ) )
      };
      getCurrentTimetableStub= sinon.stub().returns( Promise.resolve( [ mockOldTimetable ] ) );
      buildNewTimetableStub= sinon.stub().returns( Promise.resolve( mockNewTimetable  ) );
      helper.__set__('buildNewTimetable', buildNewTimetableStub );
      helper.__set__('getCurrentTimetable', getCurrentTimetableStub );
    });

    it('should retrieve the currently active timetable', function( done ){
      helper.setNewTimetable().then( function(){
        getCurrentTimetableStub.should.have.been.called;
        done();
      }).catch( done );
    });
    it('should retrieve a new timetable', function( done ){
      helper.setNewTimetable().then( function(){
        buildNewTimetableStub.should.have.been.called;
        done();
      }).catch( done );
    });
    it('should unset the old timetable "current" status & save', function( done ){
      helper.setNewTimetable().then( function(){
        mockOldTimetable.current.should.be.eql( false );
        mockOldTimetable.should.include.key ( 'unsetAt' );
        mockOldTimetable.unsetAt.should.be.a( 'Date' );
        mockOldTimetable.save.should.be.called;
        done();
      }).catch( done );
    });
    it('should set the new timetable "current" status & save', function( done ){
      helper.setNewTimetable().then( function(){
        mockNewTimetable.current.should.be.eql( true );
        mockNewTimetable.should.include.key ( 'setAt' );
        mockNewTimetable.save.should.be.called;
        done();
      }).catch( done );
    });
    it('should return the newly saved current timetable', function( done ){
      helper.setNewTimetable().then( function( timetable ){
        timetable.should.be.eql( savedTimetable[ 0 ] );
        done();
      }).catch( done );
    });
  });

});

